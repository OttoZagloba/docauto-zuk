 
#!/bin/bash
# Copyright (c) 2019 Otto Zagłoba
#      http://zukowka.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# requires: imagemagick

for pdf in ../*.pdf; do
    echo "${pdf##*/}:"
    # skip if the thumbnail exists and is newer than the PDF
    [ -e "${pdf##*/}".png ] && [ "${pdf##*/}".png -nt "$pdf" ] && echo -e "\t thumbnail is newer" && continue
    
    # get info about the pdf
    echo -e "\tget info"
    pages="`qpdf --show-npages "$pdf"` p."
    size=`ls -sh "$pdf" | cut -f 1 -d " "`
    
    # create and icon the small download icon with page count and size
    echo -e "\tcreate icon"
    icon=`mktemp --suffix .png`
    convert  \( icon-download.png -resize 200x200  \) -pointsize 32 -background Orange  label:"$size / $pages" +swap -gravity Center -append $icon
    echo -e "\tcreate thumbnail"
    convert -thumbnail 700x "$pdf"[0] -background white -alpha remove $icon  -composite "${pdf##*/}".png
    rm $icon
done


# EOF
