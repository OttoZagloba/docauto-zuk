all:
	-ssh-add ~/.ssh/id_docauto
	-git pull
	-git add *
	-git commit -am '.'
	-git push
	-ssh-add -D

ia:
	cd references/ && ia upload zukowka-zuk-spare-parts-catalog ZUKOWKA-Zuk_spare_parts_catalog-LQ.pdf ZUKOWKA-Zuk_spare_parts_catalog.pdf

# git clone git@gitlab.com:OttoZagloba/docauto-zuk.git
