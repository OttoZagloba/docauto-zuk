#!/bin/bash
# Copyright (c) 2019-2024 Otto Zagłoba
#      http://zukowka.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# requires: pdftk imagemagick qpdf ghostscript beep nano


# skip  editing titles when missing
#NOEDIT=1

# cleanup fonctions
TMP=
on_exit() {
    echo "cleaning up:"
    for file in $TMP; do
	[ -f $file ] && rm -fv $file
	[ -d $file ] && rm -fvr $file
    done
}
trap on_exit EXIT HUP INT QUIT TERM STOP PWR  # 1 2 3 15 30

CPU_COUNT=`awk '/^processor/{n+=1}END{print n}' /proc/cpuinfo`
LQ_SETTING="-dDownsampleColorImages=true -dDownsampleGrayImages=true -dDownsampleMonoImages=true -dPDFSETTINGS=/ebook"  # /ebook = 150 dpi  /screen = 72 dpi

# create blank A4
TMP_A4BLANK=$(mktemp --suffix=.pdf)
TMP="$TMP $TMP_A4BLANK"
convert -page a4 canvas:transparent $TMP_A4BLANK
mogrify -rotate 90 $TMP_A4BLANK

# create bookmarks
TMP_BOOKMARKS=$(mktemp)
TMP="$TMP $TMP_BOOKMARKS"
echo "InfoBegin
InfoKey: Title
InfoValue: Zuk spare parts catalog [ZUKOWKA]

BookmarkBegin
BookmarkTitle: Consumables/Torque
BookmarkLevel: 1	 	
BookmarkPageNumber: 2" > $TMP_BOOKMARKS
	    

# create new pdf per page
TMP_DIR=$(mktemp --directory)
TMP_DIR_LQ=$(mktemp --directory)
TMP="$TMP $TMP_DIR $TMP_DIR_LQ"
pagecount=3
echo "$TMP_DIR $TMP_DIR_LQ (LQ)"
for dir in *; do
    [ ! -d "$dir" ] && continue
    ((pagecount++))
    prefix=`echo "$dir" | cut -c 1`
    title=`echo $dir | cut -c 3-`
    start=`date +%s`
    echo "$dir"
    echo "BookmarkBegin
BookmarkTitle: $title
BookmarkLevel: 1
BookmarkPageNumber: $pagecount" >> $TMP_BOOKMARKS
    convert -density 300 $TMP_A4BLANK[0] -pointsize 24 -font Arial-Gras -gravity center -draw "text 0,0 '${title^^}'" $TMP_DIR/$prefix-0000.pdf || break
    gs -dNumRenderingThreads=$CPU_COUNT -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.6 $LQ_SETTING -sOutputFile=$TMP_DIR_LQ/$prefix-0000.pdf $TMP_DIR/$prefix-0000.pdf || break

    for page in "$dir"/*.pdf; do
	# break this loop if any important command fails
	((pagecount++))
	previouspagename="$pagename"
	# page name is only the real original number, left from - or _ in the filename
	pagename=`basename "$page" .pdf | cut -f 1 -d \- | cut -f 1 -d \_`	
	# specificity is anything after _ in the filename, left from - if existing
	case `basename "$page" .pdf | cut -f 2 -d \_|  cut -f 1 -d \- ` in
	    "S21")
		pagedescriptionfile="$dir"/"$pagename"_"S21".txt
		specificto="S-21 "
		specificto_color="#00880a"
		;;
	    "4C90")
		pagedescriptionfile="$dir"/"$pagename"_"4C90".txt
		specificto="4C90 "
		specificto_color="#b85f00"
		;;
	    "4CTI90")
		pagedescriptionfile="$dir"/"$pagename"_"4CTI90".txt
		specificto="4CTi90 "
		specificto_color="#983000"
		;;
	    "TS521")
		pagedescriptionfile="$dir"/"$pagename"_"TS521".txt
		specificto="TS5-21 "
		specificto_color="#6500b4"
		;;
	    "ALT")
		pagedescriptionfile="$dir"/"$pagename"_"ALT".txt
		specificto="(alt) "
		specificto_color="#4682b4"
		;;
	    *)
		pagedescriptionfile="$dir"/"$pagename".txt
		specificto=
		specificto_color="black"
		;;
	esac

	# find potential english description
	pagedescription=
	if [ -e "$pagedescriptionfile" ]; then
	    # slurp it (ignore comments line that with #
	    pagedescription=`grep -v ^\# "$pagedescriptionfile"`
	else
	    # if no page description exists yet, suggest the user the longer line with all caps characters
	    # in each page (likely to be the real title)
	    for gettitle in "$dir"/"$pagename"*.pdf; do
		echo "get possible title from $gettitle..."
		pdftotext "$gettitle" - | sed 's/[a-z0-9[:punct:]]//g' | sed 's/^\s*$//g' | grep -v ^$ | awk '{ if (length($0) > max) {max = length($0); maxline = $0} } END { print maxline }' | tr '[:upper:]' '[:lower:]' | sed 's/^/# /' >> "$pagedescriptionfile"
	    done
	    # then let the user edit it (unless NOEDIT is set)
	    [ -z "$NOEDIT" ] && beep -f 70 -l 15
	    [ -z "$NOEDIT" ] && nano "$pagedescriptionfile"
	    # then slurp it
	    pagedescription=`grep -v ^\# "$pagedescriptionfile"`
	fi
	     
	TMP_HEAD=$(mktemp --suffix=.pdf)
	TMP="$TMP $TMP_HEAD"	
	echo "$prefix $pagename ($pagecount) $specificto$pagedescription ($pagedescriptionfile); $TMP_HEAD"

	# bookmark only the first occurence of the page
	if [ "$previouspagename" != "$pagename" ]; then
	    echo "BookmarkBegin
BookmarkTitle: $specificto$pagedescription
BookmarkLevel: 2	 	
BookmarkPageNumber: $pagecount" >> $TMP_BOOKMARKS
	fi
	# generate header
	convert -density 300 $TMP_A4BLANK[0] -pointsize 13 -draw "text 10,50 '$title - $pagedescription'" -fill "$specificto_color" -draw "text 60,110 '$specificto'" $TMP_HEAD || break

	# merge page with header
	pdftk $TMP_HEAD background "$page" output $TMP_DIR/$prefix-`basename "$page" .pdf`.pdf || break
	# make low quality copy (per small PDF because ghostscript is very demanding on a big final PDF)
	# (-dFIXEDMEDIA to avoid page re-oriented)
	gs -dNumRenderingThreads=$CPU_COUNT -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dFIXEDMEDIA -dCompatibilityLevel=1.6 $LQ_SETTING -sOutputFile=$TMP_DIR_LQ/$prefix-`basename "$page" .pdf`.pdf $TMP_DIR/$prefix-`basename "$page" .pdf`.pdf || break
	rm -f $TMP_HEAD
    done
    echo $((`date +%s`-start))"s"
    du -sh $TMP_DIR/
    du -sh $TMP_DIR_LQ/
    echo " "
done

# hand made covers
TMP_HEAD=$(mktemp --suffix=.pdf)
TMP="$TMP $TMP_HEAD"
TMP_DIR_INDEX=$(mktemp --directory)
TMP="$TMP $TMP_DIR_INDEX"
echo "index ; $TMP_HEAD $TMP_DIR_INDEX"
start=`date +%s`
convert -density 300 $TMP_A4BLANK[0] -pointsize 13 -draw "text 10,50 '`date +%Y%m%d`'" $TMP_HEAD
libreoffice --convert-to pdf --outdir $TMP_DIR_INDEX index.odt
pdftk $TMP_DIR_INDEX/index.pdf cat 1 1 output $TMP_DIR_INDEX/index1.pdf
pdftk $TMP_DIR_INDEX/index.pdf cat 2 2 output $TMP_DIR_INDEX/index2.pdf
pdftk $TMP_HEAD background $TMP_DIR_INDEX/index1.pdf output $TMP_DIR/0000-index.pdf
gs -dNumRenderingThreads=$CPU_COUNT -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.6 $LQ_SETTING -sOutputFile=$TMP_DIR_LQ/0000-index.pdf $TMP_DIR/0000-index.pdf
pdftk $TMP_HEAD background $TMP_DIR_INDEX/index2.pdf output $TMP_DIR/ZZZZ-index.pdf
gs -dNumRenderingThreads=$CPU_COUNT -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.6 $LQ_SETTING -sOutputFile=$TMP_DIR_LQ/ZZZZ-index.pdf $TMP_DIR/ZZZZ-index.pdf
cp $TMP_DIR/ZZZZ-index.pdf $TMP_DIR_LQ/
for ref in refs-*.odt; do
    libreoffice --convert-to pdf --outdir $TMP_DIR_INDEX "$ref"
    mv $TMP_DIR_INDEX/`basename "$ref" .odt`.pdf $TMP_DIR/0005-`basename "$ref" .odt`.pdf
    gs -dNumRenderingThreads=$CPU_COUNT -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.6 $LQ_SETTING -sOutputFile=$TMP_DIR_LQ/0005-`basename "$ref" .odt`.pdf $TMP_DIR/0005-`basename "$ref" .odt`.pdf    
done
echo $((`date +%s`-start))"s"
echo " "
rm -rf $TMP_HEAD $TMP_DIR_INDEX

# merge pdf
TMP_MERGE=$(mktemp --suffix=.pdf)
TMP_MERGE_LQ=$(mktemp --suffix=.pdf)
TMP="$TMP $TMP_MERGE $TMP_MERGE_LQ"
echo "merge $TMP_DIR/* ; $TMP_MERGE $TMP_MERGE_LQ"
start=`date +%s`
pdftk $TMP_DIR/*.pdf cat output $TMP_MERGE
pdftk $TMP_DIR_LQ/*.pdf cat output $TMP_MERGE_LQ
echo $((`date +%s`-start))"s"
ls -sh $TMP_MERGE $TMP_MERGE_LQ

# add  bookmarks
TMP_NONLINEAR=$(mktemp --suffix=.pdf)
TMP_NONLINEAR_LQ=$(mktemp --suffix=.pdf)
TMP="$TMP $TMP_NONLINEAR $TMP_NONLINEAR_LQ"
echo "add booksmarks $TMP_BOOKMARKS ; $TMP_NONLINEAR $TMP_NONLINEAR_LQ"
start=`date +%s`
pdftk $TMP_MERGE update_info $TMP_BOOKMARKS output $TMP_NONLINEAR
pdftk $TMP_MERGE_LQ update_info $TMP_BOOKMARKS output $TMP_NONLINEAR_LQ
echo $((`date +%s`-start))"s" 
ls -sh $TMP_NONLINEAR $TMP_NONLINEAR_LQ
rm -f $TMP_MERGE $TMP_MERGE_LQ $TMP_BOOKMARKS

# linearize
echo "linearize $TMP_NONLINEAR $TMP_NONLINEAR_LQ"
start=`date +%s`
qpdf --linearize $TMP_NONLINEAR ../ZUKOWKA-Zuk_spare_parts_catalog.pdf
qpdf --linearize $TMP_NONLINEAR_LQ ../ZUKOWKA-Zuk_spare_parts_catalog-LQ.pdf
echo $((`date +%s`-start))"s"
ls -sh ../ZUKOWKA-Zuk_spare_parts_catalog*.pdf
rm -f $TMP_NONLINEAR $TMP_NONLINEAR_LQ

# EOF
